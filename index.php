<?php
/**
 * Plugin Name: MFP GeoIP
 * Plugin URI: https://hypernova.digital/
 * Description: the Description will be set when ready for production.
 * Version: 1.0
 * Author: hypernova digital
 * Author URI: https://hypernova.digital/
 * 
 */
require_once dirname( __FILE__ ) . '/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'mfp_register_required_plugins' );
function mfp_register_required_plugins() {
    $plugins = array(
        array(
            'name' => 'Advanced Custom Fields',
            'slug'  => 'advanced-custom-fields-pro',
        )
        );
    $config = array(
        'id' => 'mfpgeo',
        'default_path'  => '',
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'plugins.php',            // Parent menu slug.
		'capability'   => 'manage_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',  
    );
    tgmpa( $plugins, $config );
}

include __DIR__ . '/src/mfp-geoip.php';

