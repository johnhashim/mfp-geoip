<?php

//incluse the form shortcode
  include __DIR__ . '/form-shortcode.php';

/**
 * api queries
 * 
 */

add_action( 'rest_api_init', function () {
  register_rest_route( 'mfp/v1', '/email', array(
    'methods' => 'POST',
    'callback' => 'geoIp',
  ) );
} );

function geoIP( $data ) {
  // wp data
    $formData = $data->get_params();
    $headers = $data->get_headers();

    $data_for_segment = [];
    
    /* Looping through an array*/
    foreach ($formData as $key => $value){  
      $data_for_segment[$key] = $value;  
    } 

    //get the ip adreess
    $data_for_segment['ip'] = $_SERVER['REMOTE_ADDR'];

    
    //get geoIp data from cloudRun
    $geoIpInfo = json_decode(getGeoIpInfo($data_for_segment['ip'])['body']);

    foreach ($geoIpInfo as $key => $value){  
      $data_for_segment[$key] = $value;  
    } 

    $segmentKey = get_field('segment_key', 'option');
    $segmentUrl = get_field('segment_url', 'option');
    $response = wp_remote_post( $segmentUrl, array(
      'body'    => $data_for_segment,
      'headers' => array(
          'Authorization' => 'Basic ' , $segmentKey,
      ),
  ) );

    // return successful WP rest response
    
    return $response;
    //return var_dump($_SERVER['REMOTE_ADDR']);



}

function getGeoIpInfo($ip) {
  
  $endpoint = get_field('cloud_run_url', 'option');
  $options = [
    'body'        => $ip,
    'headers'     => [
        'Content-Type' => 'text/plain',
    ],
    'timeout'     => 60,
    'redirection' => 5,
    'blocking'    => true,
    'httpversion' => '1.0',
    'sslverify'   => false,
    'data_format' => 'body',
];
  $response = wp_remote_post($endpoint, $options);
  return $response;

  
}




/**
 * Create theme optoins page
 */

if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page(
		array(
			'page_title' => 'MFP GeoIP Settings',
			'menu_title' => 'GeoIP Settings',
			'menu_slug'  => 'mfp-geoip-settings',
			'capability' => 'edit_posts',
			'redirect'   => false,
		)
		);
}


if( function_exists('acf_add_local_field_group') ):

  acf_add_local_field_group(array(
    'key' => 'group_5e273c2360fa8',
    'title' => 'MFP GeoIp',
    'fields' => array(
      array(
        'key' => 'field_5e273c3b1cfde',
        'label' => 'Segment Url',
        'name' => 'segment_url',
        'type' => 'text',
        'instructions' => 'Add your Segment Url in the field',
        'required' => 1,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_5e273c8c1cfdf',
        'label' => 'Segment key',
        'name' => 'segment_key',
        'type' => 'text',
        'instructions' => 'Add your Segment key in the field',
        'required' => 1,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
        'prepend' => '',
        'append' => '',
        'maxlength' => '',
      ),
      array(
        'key' => 'field_5e273d4e1cfe0',
        'label' => 'Cloud run Url',
        'name' => 'cloud_run_url',
        'type' => 'url',
        'instructions' => 'Add your cloud run GeoIP here',
        'required' => 1,
        'conditional_logic' => 0,
        'wrapper' => array(
          'width' => '',
          'class' => '',
          'id' => '',
        ),
        'default_value' => '',
        'placeholder' => '',
      ),
    ),
    'location' => array(
      array(
        array(
          'param' => 'options_page',
          'operator' => '==',
          'value' => 'mfp-geoip-settings',
        ),
      ),
    ),
    'menu_order' => 0,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => '',
    'active' => true,
    'description' => '',
  ));
  
  endif;