
<?php
/**
 *this is the form shortcode
 * 
 */

function mfp_scripts() {
    //wp_register_script( 'jquery', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/core.js');
    //wp_enqueue_script('jquery');
    wp_enqueue_style('main_css', plugins_url('main.css',__FILE__ ));
    wp_enqueue_script( 'main_js', plugins_url('main.js',__FILE__ ), array('jquery'), false, true );
}
add_action( 'wp_enqueue_scripts','mfp_scripts');


function mfp_geoIP() {
    ?>
        <form class="email-signup">
            <div class="group">
                <input name="email" class="email-input" placeholder="Email Address" id="email" type="email" required >
            </div>
            <div class="group">
            <button type="submit"  class="button submit">Subscribe</button>
            </div>
            <div class="message">Thank you for reaching out to us. We'll get back to you shortly!</div>
        </form>
    <?php
}
add_shortcode('emailform', 'mfp_geoIP');
